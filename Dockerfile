FROM node:16-alpine

# 设置时区
ENV TZ=Asia/Shanghai \
    DEBIAN_FRONTEND=noninteractive

#参数，node的环境为生产环境
ENV NODE_ENV=production

#容器内创建目录blog
RUN mkdir -p /app

# 指定工作目录
WORKDIR /app

# 复制当前代码到/app工作目录
COPY ./ ./

# npm 安装依赖
COPY package.json /app/package.json
RUN cd /app && rm -rf /app/node_modules &&  npm install

#设置外部访问端口
EXPOSE 3000

# 运行命令以启动应用程序
CMD [ "node", "dist/main" ]
