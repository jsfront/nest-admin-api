import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Index('roleperm', ['roleId', 'permissionId'], { unique: true })
@Index('fk_rolepermission_permission', ['permissionId'], {})
@Entity('auth_role_permission', { schema: 'vue-admin' })
export class AuthRolePermission extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('int', { name: 'role_id', nullable: true, unsigned: true })
  roleId: number | null

  @Column('int', { name: 'permission_id', nullable: true, unsigned: true })
  permissionId: number | null

  @Column('int', { name: 'app_id', nullable: true, comment: '应用id' })
  appId: number | null

  @Column('datetime', { name: 'create_time', comment: '创建时间' })
  createTime: Date

  @Column('datetime', { name: 'update_time', comment: '修改时间' })
  updateTime: Date

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市id',
    unsigned: true,
  })
  cityId: number | null
}
