import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('tags', { schema: 'vue-admin' })
export class Tags extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number

  @Column('int', { name: 'city_id', nullable: true, unsigned: true })
  cityId: number | null

  @Column('varchar', { name: 'tag_name', length: 255 })
  tagName: string

  @Column('varchar', { name: 'thumbnail', nullable: true, length: 1023 })
  thumbnail: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
