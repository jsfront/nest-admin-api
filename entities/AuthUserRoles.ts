import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Index('roleperm', ['roleId', 'userId'], { unique: true })
@Index('FK_UserRoles_Roles_RoleId', ['roleId'], {})
@Index('FK_UserRoles_Users_UserId', ['userId'], {})
@Entity('auth_user_roles', { schema: 'vue-admin' })
export class AuthUserRoles extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('int', { name: 'user_id', unsigned: true })
  userId: number

  @Column('int', { name: 'role_id', default: () => "'0'" })
  roleId: number

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市id',
    unsigned: true,
  })
  cityId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date

  @Column('datetime', {
    name: 'update_time',
    comment: '修改时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date
}
