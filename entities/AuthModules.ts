import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('auth_modules', { schema: 'vue-admin' })
export class AuthModules extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '模块编号',
    unsigned: true,
  })
  id: number

  @Column('varchar', { name: 'code', comment: 'code', length: 100 })
  code: string

  @Column('varchar', { name: 'title', comment: '模块名称', length: 50 })
  title: string

  @Column('int', {
    name: 'parent_id',
    comment: '父模块编号',
    unsigned: true,
    default: () => "'0'",
  })
  parentId: number

  @Column('varchar', {
    name: 'component',
    nullable: true,
    comment: '组件名',
    length: 100,
  })
  component: string | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
  })
  order: number | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '菜单状态',
    unsigned: true,
  })
  status: number | null

  @Column('varchar', { name: 'path', comment: '模块路径', length: 200 })
  path: string

  @Column('tinyint', { name: 'blank', comment: '是否外链', unsigned: true })
  blank: number

  @Column('tinyint', { name: 'hidden', comment: '是否隐藏', unsigned: true })
  hidden: number

  @Column('varchar', {
    name: 'href',
    nullable: true,
    comment: '链接',
    length: 100,
  })
  href: string | null

  @Column('varchar', { name: 'icon_name', nullable: true, length: 50 })
  iconName: string | null

  @Column('varchar', {
    name: 'target',
    nullable: true,
    comment: '链接方式',
    length: 100,
  })
  target: string | null

  @Column('int', { name: 'city_id', nullable: true, comment: '城市id' })
  cityId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', { name: 'create_time', comment: '创建时间' })
  createTime: Date

  @Column('datetime', { name: 'update_time', comment: '修改时间' })
  updateTime: Date
}
