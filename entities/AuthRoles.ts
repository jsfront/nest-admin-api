import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Index('IndexUnique_Name', ['name'], { unique: true })
@Entity('auth_roles', { schema: 'vue-admin' })
export class AuthRoles extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '用户组编号',
    unsigned: true,
  })
  id: number

  @Column('varchar', {
    name: 'name',
    unique: true,
    comment: '角色(用户组)名称',
    length: 50,
  })
  name: string

  @Column('char', {
    name: 'status',
    nullable: true,
    comment: '角色状态',
    length: 10,
  })
  status: string | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
    default: () => "'0'",
  })
  order: number | null

  @Column('varchar', {
    name: 'remark',
    nullable: true,
    comment: '注释',
    length: 200,
  })
  remark: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date

  @Column('datetime', {
    name: 'update_time',
    comment: '修改时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date

  @Column('int', { name: 'city_id', nullable: true, comment: '城市id' })
  cityId: number | null
}
