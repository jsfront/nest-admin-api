import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('backend', { schema: 'vue-admin' })
export class Backend extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('varchar', { name: 'name', comment: '语言名称', length: 100 })
  name: string

  @Column('varchar', {
    name: 'url',
    nullable: true,
    comment: '链接',
    length: 100,
  })
  url: string | null

  @Column('int', {
    name: 'type',
    nullable: true,
    comment: '类型',
    unsigned: true,
  })
  type: number | null

  @Column('int', { name: 'app_id', nullable: true, unsigned: true })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    comment: '更新时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
