import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UserModule } from './modules/user/user.module'
import { MenuModule } from './modules/menu/menu.module'
import { AuthModule } from './modules/auth/auth.module'
import { RoleModule } from './modules/role/role.module'
import { ArticleModule } from './modules/article/article.module'
import { CategoryModule } from './modules/category/category.module'
import { TagsModule } from './modules/tags/tags.module'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { getConfig } from './shared/utils'
import { TypeOrmModule } from '@nestjs/typeorm'
import { MyLogger } from './common/logger/logger.service'
import { NavModule } from './modules/nav/nav.module'
import { FileModule } from './modules/file/file.module'
import { AppInfoModule } from './modules/app/app.module'
import { BackendModule } from './modules/backend/backend.module'

@Module({
  imports: [
    // 配置模块
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
      load: [getConfig],
    }),
    // 数据库
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const { host, port, username, password, database } = configService.get('DB')

        return {
          type: 'mysql', // 数据库类型
          host, // 主机，默认为localhost
          port, // 端口号
          username, // 用户名
          password, // 密码
          database, // 数据库名
          logging: true, // 开启所有数据库信息打印
          // timezone: '+08:00', // 服务器上配置的时区
          timezone: 'Z',
          synchronize: false, // 根据实体自动创建数据库表， 生产环境建议关闭
          // entities: [`${__dirname}/**/*.entity{.ts,.js}`], // mac 系统支持这种
          autoLoadEntities: true, // 自动加载实体 // windows 系统支持这种
        }
      },
      inject: [ConfigService],
    }),
    FileModule,
    UserModule,
    AuthModule,
    MenuModule,
    RoleModule,
    ArticleModule,
    CategoryModule,
    TagsModule,
    NavModule,
    AppInfoModule,
    BackendModule,
  ],
  controllers: [AppController],
  providers: [AppService, MyLogger],
})
export class AppModule {}
