import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'
// import { IParamsResult, ListParams } from '@/shared/core/decorator/ListParams.decorator'
import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common'
import { ApiResponse } from '@nestjs/swagger'
import { ArticleService } from './article.service'
import { CreateArticleDto } from './dto/create-article.dto'
import { UpdateArticleDto } from './dto/update-article.dto'
import { ArticleEntity } from './entities/article.entity'

@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Post()
  @ApiResponse({ status: 201, description: '创建文章', type: [ArticleEntity] })
  create(@Body() createArticleDto: CreateArticleDto) {
    return this.articleService.create(createArticleDto)
  }

  @Post('/add')
  @ApiResponse({ status: 201, description: '创建文章', type: [ArticleEntity] })
  add(@Body() createArticleDto: CreateArticleDto) {
    return this.articleService.create(createArticleDto)
  }

  @Get()
  // async findAll(@ListParams({ where: ['title', { field: 'version', condition: 'Equal' }, { field: 'navId', condition: 'Equal' }], order: { id: 'DESC' } }) params: IParamsResult) {
  async findAll(@Query() queryParams) {
    const [rows, total] = await this.articleService.findAll(queryParams)
    const { page, limit } = queryParams

    return {
      rows,
      page: { total, page, limit },
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.articleService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateArticleDto: UpdateArticleDto) {
    return this.articleService.update(+id, updateArticleDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.articleService.remove(id)
  }
}
