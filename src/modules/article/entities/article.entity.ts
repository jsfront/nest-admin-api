import { CategoryEntity } from '@/modules/category/entities/category.entity'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { TagsEntity } from '@/modules/tags/entities/tag.entity'
import { BackendEntity } from '@/modules/backend/entities/backend.entity'
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from 'typeorm'

@Entity('article_info')
export class ArticleEntity extends CommonEntity {
  @Column('int', { name: 'city_id', comment: '城市 id', unsigned: true })
  cityId: number

  @Column('int', { name: 'category_id', comment: '分类 id', unsigned: true })
  categoryId: number

  @Column('int', { name: 'user_id', comment: '用户 id', unsigned: true })
  userId: number

  @Column('int', { name: 'nav_id', comment: '栏目 id', unsigned: true })
  navId: number

  @Column('varchar', { name: 'title', comment: '标题', length: 255 })
  title: string

  @Column('text', { name: 'content', nullable: true, comment: '内容' })
  content: string | null

  @Column('text', { name: 'md', comment: 'markdown' })
  md: string

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '文章状态',
    width: 1,
  })
  status: number

  @Column('varchar', {
    name: 'keywords',
    nullable: true,
    comment: '关键字',
    length: 100,
  })
  keywords: string | null

  @Column('varchar', {
    name: 'cover_url',
    nullable: true,
    comment: '缩略图',
  })
  coverUrl: string | null

  @Column('int', {
    name: 'views_count',
    nullable: true,
    comment: '浏览量',
    unsigned: true,
  })
  viewsCount: number

  @Column('int', {
    name: 'like_count',
    nullable: true,
    comment: '点赞数',
    unsigned: true,
  })
  likeCount: number | null

  @Column('int', {
    name: 'comment_count',
    nullable: true,
    comment: '评论数',
    unsigned: true,
  })
  commentCount: number | null

  @Column('varchar', {
    name: 'summary',
    nullable: true,
    comment: '摘要',
    length: 1000,
  })
  summary: string | null

  @Column('smallint', { name: 'is_top', nullable: true, comment: '是否置顶' })
  isTop: number | null

  @ManyToMany(() => TagsEntity, (tag) => tag.articles)
  @JoinTable({
    name: 'article_tags',
    joinColumns: [{ name: 'article_id' }],
    inverseJoinColumns: [{ name: 'tags_id' }],
  })
  tags: TagsEntity[]

  @ManyToMany(() => BackendEntity, (backend) => backend.articles)
  @JoinTable({
    name: 'article_backend',
    joinColumns: [{ name: 'article_id' }],
    inverseJoinColumns: [{ name: 'backend_id' }],
  })
  backend: BackendEntity[]

  @Column('varchar', {
    name: 'author',
    nullable: true,
    comment: '作者',
    length: 30,
  })
  author: string | null

  @Column('varchar', {
    name: 'version',
    comment: '版本',
    length: 10,
  })
  version: string | null

  @Column('varchar', {
    name: 'github',
    comment: 'github',
    length: 100,
  })
  github: string | null

  @Column('varchar', {
    name: 'gitee',
    comment: 'gitee',
    length: 100,
  })
  gitee: string | null

  @Column('varchar', {
    name: 'homepage',
    comment: '个人主页',
    length: 100,
  })
  homepage: string | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
  })
  order: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @ManyToOne((type) => CategoryEntity, (category: any) => category.article)
  @JoinColumn({ name: 'category_id' })
  category: CategoryEntity
}
