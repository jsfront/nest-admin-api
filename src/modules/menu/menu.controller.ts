import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'
import { MenuService } from './menu.service'
import { CreateMenuDto } from './dto/create-menu.dto'
import { UpdateMenuDto } from './dto/update-menu.dto'

@Controller('menu')
export class MenuController {
  constructor(private readonly menuService: MenuService) {}

  @Post()
  create(@Body() createMenuDto: CreateMenuDto) {
    return this.menuService.create(createMenuDto)
  }

  @Get()
  async findAll() {
    const rows = await this.menuService.findAll()
    return { rows, page: { total: rows.length } }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.menuService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMenuDto: UpdateMenuDto) {
    return this.menuService.update(+id, updateMenuDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.menuService.remove(id)
  }
}
