import { Column, Entity, ManyToMany } from 'typeorm'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { ApiProperty } from '@nestjs/swagger'
import { ArticleEntity } from '@/modules/article/entities/article.entity'

@Entity('backend')
export class BackendEntity extends CommonEntity {
  @Column('varchar', { name: 'backend_name', comment: '语言名称', length: 100 })
  backendName: string

  @Column('varchar', {
    name: 'backend_url',
    nullable: true,
    comment: '链接',
    length: 100,
  })
  backendUrl: string | null

  @Column('int', {
    name: 'type',
    nullable: true,
    comment: '类型',
    unsigned: true,
  })
  type: number | null

  @Column('int', { name: 'app_id', nullable: true, unsigned: true })
  appId: number | null

  @ApiProperty()
  @ManyToMany(() => ArticleEntity, (article) => article.backend)
  articles: ArticleEntity[]
}
