import { Module } from '@nestjs/common'
import { BackendService } from './backend.service'
import { BackendController } from './backend.controller'
import { BackendEntity } from './entities/backend.entity'
import { TypeOrmModule } from '@nestjs/typeorm'

@Module({
  imports: [TypeOrmModule.forFeature([BackendEntity])],
  controllers: [BackendController],
  providers: [BackendService],
})
export class BackendModule {}
