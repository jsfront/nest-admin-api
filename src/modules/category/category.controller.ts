import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'
import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { CategoryService } from './category.service'
import { CreateCategoryDto } from './dto/create-category.dto'
import { UpdateCategoryDto } from './dto/update-category.dto'
import { IParamsResult, ListParams } from '@/shared/core/decorator/ListParams.decorator'

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post()
  create(@Body() createCategoryDto: CreateCategoryDto) {
    return this.categoryService.create(createCategoryDto)
  }

  @Get()
  async findAll(@ListParams({ where: ['appId', 'navId'] }) params: IParamsResult) {
    const rows = await this.categoryService.findAll(params)
    return { rows }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.categoryService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto) {
    return this.categoryService.update(+id, updateCategoryDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.categoryService.remove(id)
  }
}
