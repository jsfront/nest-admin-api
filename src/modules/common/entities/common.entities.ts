import { TimestampTransformer } from '@/shared/transformer/timestamp.transformer'
import { BeforeInsert, BeforeUpdate, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

export class CommonEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', comment: 'id', unsigned: true })
  id: number

  @CreateDateColumn({ name: 'create_time', comment: '创建时间', default: () => 'CURRENT_TIMESTAMP(6)', transformer: new TimestampTransformer() })
  createTime: Date

  @UpdateDateColumn({ name: 'update_time', comment: '修改时间', default: () => 'CURRENT_TIMESTAMP(6)', transformer: new TimestampTransformer(), select: true })
  updateTime: Date

  @BeforeInsert()
  createTimeHooks() {
    this.createTime = new Date()
    this.updateTime = new Date()
  }

  @BeforeUpdate()
  updateTimeHooks() {
    this.updateTime = new Date()
  }
}
