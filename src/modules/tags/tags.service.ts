import { IParamsResult } from '@/shared/core/decorator/ListParams.decorator'
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateTagDto } from './dto/create-tag.dto'
import { UpdateTagDto } from './dto/update-tag.dto'
import { TagsEntity } from './entities/tag.entity'

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(TagsEntity)
    private readonly tagRepository: Repository<TagsEntity>,
  ) {}

  async create(dto: CreateTagDto) {
    const result = await this.tagRepository.create(dto)
    await this.tagRepository.save(result)

    return result
  }

  findAll({ where, order }: IParamsResult) {
    return this.tagRepository.find({ order, where })
  }

  // 详情
  findOne(id) {
    return this.tagRepository.findOne({ where: { id } })
  }

  // 修改
  async update(id, dto: UpdateTagDto) {
    const old = await this.findOne(id)
    const vo = await this.tagRepository.merge(old, dto)

    return this.tagRepository.save(vo)
  }

  // 删除
  async remove(id) {
    const result = await this.tagRepository.delete(id)
    Logger.log(`删除返回数据：${JSON.stringify(result)}`)

    return result.affected || new HttpException('无法删除，请稍候在试', HttpStatus.BAD_REQUEST)
  }
}
