import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'
import { IParamsResult, ListParams } from '@/shared/core/decorator/ListParams.decorator'
import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { AppService } from './app.service'
import { CreateAppDto } from './dto/create-app.dto'
import { UpdateAppDto } from './dto/update-app.dto'

@Controller('app')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  create(@Body() createAppDto: CreateAppDto) {
    return this.appService.create(createAppDto)
  }

  @Get()
  async findAll(@ListParams({ where: ['title'], order: { id: 'DESC' } }) params: IParamsResult) {
    const [rows, total] = await this.appService.findAll(params)
    const { pageParams } = params

    return {
      rows,
      page: { total, ...pageParams },
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.appService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAppDto: UpdateAppDto) {
    return this.appService.update(+id, updateAppDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.appService.remove(id)
  }
}
