import { CommonEntity } from '@/modules/common/entities/common.entities'
import { MenuEntity } from '@/modules/menu/entities/menu.entity'
import { UserEntity } from '@/modules/user/entities/user.entity'
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm'

@Entity('auth_roles')
export class RoleEntity extends CommonEntity {
  @Column('varchar', {
    name: 'name',
    unique: true,
    comment: '角色(用户组)名称',
    length: 50,
  })
  name: string

  @Column('char', {
    name: 'status',
    nullable: true,
    comment: '角色状态',
    length: 10,
  })
  status: string | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
    default: () => "'0'",
  })
  order: number | null

  @Column('varchar', {
    name: 'remark',
    nullable: true,
    comment: '注释',
    length: 200,
  })
  remark: string | null

  @Column('int', { name: 'city_id', nullable: true, comment: '店铺id' })
  cityId: number | null

  @ManyToMany((type) => UserEntity, (user: any) => user.roles)
  users: UserEntity[]

  @ManyToMany((type) => MenuEntity, (menu) => menu.roles)
  @JoinTable({
    name: 'auth_role_modules',
    joinColumns: [{ name: 'role_id' }],
    inverseJoinColumns: [{ name: 'module_id' }],
  })
  menus: MenuEntity[]
}
