import { Column, Entity, JoinTable, ManyToMany } from 'typeorm'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { CategoryEntity } from '@/modules/category/entities/category.entity'
import { ApiProperty } from '@nestjs/swagger'

@Entity('nav')
export class NavEntity extends CommonEntity {
  @Column('varchar', {
    name: 'name',
    nullable: true,
    comment: '名称',
    length: 100,
  })
  name: string | null

  @Column('varchar', {
    name: 'title',
    nullable: true,
    comment: '中文名称',
    length: 100,
  })
  title: string | null

  @Column('int', { name: 'parent_id', nullable: true })
  parentId: number | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
  })
  order: number | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '状态',
    width: 1,
  })
  status: boolean | null

  @Column('varchar', {
    name: 'small_img',
    nullable: true,
    comment: '封面图片',
    length: 100,
  })
  smallImg: string | null

  @Column('int', { name: 'city_id', nullable: true })
  cityId: number | null

  @Column('int', { name: 'app_id', nullable: true })
  appId: number | null

  @ApiProperty()
  @ManyToMany(() => CategoryEntity, (category) => category.navs)
  @JoinTable({
    name: 'nav_category',
    joinColumns: [{ name: 'nav_id' }],
    inverseJoinColumns: [{ name: 'category_id' }],
  })
  categories: CategoryEntity[]
}
