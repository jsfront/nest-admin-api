import { Module } from '@nestjs/common'
import { NavService } from './nav.service'
import { NavController } from './nav.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { NavEntity } from './entities/nav.entity'

@Module({
  imports: [TypeOrmModule.forFeature([NavEntity])],
  controllers: [NavController],
  providers: [NavService],
})
export class NavModule {}
