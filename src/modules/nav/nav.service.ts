import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateNavDto } from './dto/create-nav.dto'
import { UpdateNavDto } from './dto/update-nav.dto'
import { NavEntity } from './entities/nav.entity'

@Injectable()
export class NavService {
  constructor(
    @InjectRepository(NavEntity)
    private readonly navRepository: Repository<NavEntity>,
  ) {}

  async create(dto: CreateNavDto) {
    const result = await this.navRepository.create(dto)
    await this.navRepository.save(result)

    return result
  }

  findAll(title?: string) {
    const where = title?.length ? title : {}
    return this.navRepository.find({ where, relations: ['categories'], order: { order: 'ASC' } })
  }

  // 详情
  findOne(id) {
    return this.navRepository.findOne({ where: { id }, relations: ['categories'], order: { order: 'ASC' } })
  }

  // 修改
  async update(id, dto: UpdateNavDto) {
    const old = await this.findOne(id)
    const vo = await this.navRepository.merge(old, dto)

    return this.navRepository.save(vo)
  }

  // 删除
  async remove(id) {
    const result = await this.navRepository.delete(id)
    Logger.log(`删除返回数据：${JSON.stringify(result)}`)

    return result.affected || new HttpException('无法删除，请稍候在试', HttpStatus.BAD_REQUEST)
  }
}
