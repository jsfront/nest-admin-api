import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common'
import { NavService } from './nav.service'
import { CreateNavDto } from './dto/create-nav.dto'
import { UpdateNavDto } from './dto/update-nav.dto'
import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'

@Controller('nav')
export class NavController {
  constructor(private readonly navService: NavService) {}

  @Post()
  create(@Body() createNavDto: CreateNavDto) {
    return this.navService.create(createNavDto)
  }

  @Get()
  async findAll(@Query('title') title: string) {
    const rows = await this.navService.findAll(title)
    return { rows, page: { total: rows.length } }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.navService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNavDto: UpdateNavDto) {
    return this.navService.update(+id, updateNavDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.navService.remove(id)
  }
}
