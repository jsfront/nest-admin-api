import { HttpException } from '@nestjs/common'

export class AppException extends HttpException {
  code?: number
  constructor(code: number, msg = '', statusCode?: number) {
    super({ code, msg }, statusCode)
    this.code = code
    this.message = msg
  }
}
