class Type {
  CODE: number
  MESSAGE: string
}

export class ErrorCode {
  static readonly SUCCESS: Type = { CODE: 0, MESSAGE: '操作成功' }
  static readonly ERROR: Type = { CODE: 1, MESSAGE: '操作失败' }
  static readonly ParamsError: Type = { CODE: 2, MESSAGE: '参数错误' }
  static readonly DeleteError: Type = { CODE: 3, MESSAGE: '删除失败，未找到记录' }

  static readonly Unauthorized: Type = { CODE: 401, MESSAGE: '登录过期，请重新登录' }
  static readonly Forbidden: Type = { CODE: 403, MESSAGE: '没有权限执行此操作' }
  static readonly NotFound: Type = { CODE: 404, MESSAGE: '找不到请求的资源' }

  static readonly UserNameExists: Type = { CODE: 1009, MESSAGE: '用户名已存在' }

  static CodeToMessage(code: number): string {
    for (const key of Object.keys(this)) {
      return this[key].CODE === code ? this[key].MESSAGE : ''
    }
  }

  static HasCode(code: number): boolean {
    for (const key of Object.keys(this)) {
      return this[key].CODE === code
    }
  }
}
